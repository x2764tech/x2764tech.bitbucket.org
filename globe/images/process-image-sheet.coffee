gm = require 'gm'
fs = require 'fs'
xml2js = require 'xml2js'
{stderr,stdout} = process

log = (msg) ->
  stderr.write "#{JSON.stringify(msg)}\n"

labelsPath = __dirname + "/labels"

getData = ( imagePath, cb ) ->
  log "getData #{imagePath}"

  fullPath = "#{labelsPath}/#{imagePath}"
  fs.readFile fullPath, ( err, data ) ->
    
    return cb {message:"cannot read #{fullPath}", details:err} if err
    gm( fullPath )
      .on('identify', () -> log(JSON.stringify(arguments)))
      .size (err, size, stdout, stderr, cmd) ->
        log "got size "+JSON.stringify(arguments)
        log "#{fullPath} file size is #{err}, #{size}"
        return cb err if err
        cb null, size, data.toString('base64')


files = fs.readdirSync( labelsPath ).filter( (f) -> f.indexOf(".png") == f.length - 4  )

files = files.sort()
activeOrDisabledRegex = /-(active|disabled)\.png$/i
baseFiles = files.filter (f) -> !activeOrDisabledRegex.test f

fileGroups =
  for i in baseFiles
    fileName = i
    activeFile = i.replace /\.png$/i, "-active.png"
    disabledFile = i.replace /\.png$/i, "-disabled.png"
    {
      normal : fileName
      active:  activeFile if files.indexOf(activeFile) > -1
      disabled: disabledFile if files.indexOf(disabledFile) > -1
    }

simple = []

done = (err, data) ->
  log { msg: "done", err, data }
  return stderr.write "#{err}\n" if err
  for g in data
    stderr.write "outputting #{g.normal}\n"
    cssId = g.year.replace /([\s\.\&])/g, "\\$1"
    stdout.write  """
       ##{cssId}-label {
        /* #{g.normal} */
        background: url(data:image/png;base64,#{g.content}) no-repeat top;
        width: #{g.size.width}px;
        height: #{g.size.height}px;
       }
       \n"""

    stdout.write  """
        ##{cssId}-label.active {
          /* #{g.active} */
          background-image: url(data:image/png;base64,#{g.activeContent});
        }
      \n""" if g.activeContent

    stdout.write  """
        ##{cssId}-label.disabled{
          /* #{g.disabled} */
          background-image: url(data:image/png;base64,#{g.disabledContent});
        }
      \n""" if g.disabledContent

forEachAsync = (array, action, cb) ->
  items = array.slice 0
  process = (item) ->
    return cb null if not item?
    action item, (err) ->
      return cb err if err
      process items.shift()

  process items.shift()

mapAsync = (array, action, cb) ->
  results = []
  itemCb = (item, cb) ->
    action item, (err, p) ->
      if not err
        results.push if p.length == 1 then p[0] else p
      cb err

  forEachAsync array, itemCb, (err) -> cb(err, results)


processFile = (g, cb) ->

  sfiles = ( {key: key, file: g[key] } for key of g when g[key]? )

  mapped = {}
  process = (f, cb) ->
    log "\tprocessing #{f.file}"
    debugger
    getData f.file, (err, size, contents ) ->
      return cb err if err
      mapped[f.key] = { size, contents }
      cb null



  forEachAsync sfiles, process, (err ) ->
    log {g, err }
    return cb err if err
    data =
      year: g.normal.replace /\.png$/i, ""
    if g.normal?
      data.size = mapped['normal'].size
      data.content = mapped['normal'].contents
      data.normal = g.normal

    if g.active?
      data.activeSize = mapped['active'].size
      data.activeContent = mapped['active'].contents
      data.active = g.active

    if g.disabled?
      data.disabledSize = mapped['disabled'].size
      data.disabledContent = mapped['disabled'].contents
      data.disabled = g.disabled

    cb undefined , data

mapAsync fileGroups, processFile, done


