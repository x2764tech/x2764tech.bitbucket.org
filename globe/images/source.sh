#!/bin/bash


sed -i 's/fill: #DEDEDE;/fill: #000000;/gi' globe-source-hit-areas.svg 
xpath -q -e '//g[starts-with(@id, "source-")]/@id' globe-source-hit-areas.svg | sed 's/\s*id="\([^"]*\)"/\1/g' | xargs -i -n1 inkscape globe-source-hit-areas.svg -i {} -j -e labels/{}.png
sed -i 's/fill: #000000;/fill: #199aba;/gi' globe-source-hit-areas.svg 
xpath -q -e '//g[starts-with(@id, "source-")]/@id' globe-source-hit-areas.svg | sed 's/\s*id="\([^"]*\)"/\1/g' | xargs -i -n1 inkscape globe-source-hit-areas.svg -i {} -j -e labels/{}-active.png
sed -i 's/fill: #199aba;/fill: #DEDEDE;/gi' globe-source-hit-areas.svg 
xpath -q -e '//g[starts-with(@id, "source-")]/@id' globe-source-hit-areas.svg | sed 's/\s*id="\([^"]*\)"/\1/g' | xargs -i -n1 inkscape globe-source-hit-areas.svg -i {} -j -e labels/{}-disabled.png
sed -i 's/fill: #DEDEDE;/fill: #000000;/gi' globe-source-hit-areas.svg

echo "REGIONS"

sed -i 's/fill: #DEDEDE;/fill: #000000;/gi' globe-regions-hit-areas.svg
xpath -q -e '//g[starts-with(@id, "region-")]/@id' globe-regions-hit-areas.svg | sed 's/\s*id="\([^"]*\)"/\1/g' | recode html..ascii | xargs -i -n1 inkscape globe-regions-hit-areas.svg -i {} -j -e labels/{}.png
sed -i 's/fill: #000000;/fill: #199aba;/gi' globe-regions-hit-areas.svg
xpath -q -e '//g[starts-with(@id, "region-")]/@id' globe-regions-hit-areas.svg | sed 's/\s*id="\([^"]*\)"/\1/g' | recode html..ascii | xargs -i -n1 inkscape globe-regions-hit-areas.svg -i {} -j -e labels/{}-active.png
sed -i 's/fill: #199aba;/fill: #DEDEDE;/gi' globe-regions-hit-areas.svg
xpath -q -e '//g[starts-with(@id, "region-")]/@id' globe-regions-hit-areas.svg | sed 's/\s*id="\([^"]*\)"/\1/g' | recode html..ascii | xargs -i -n1 inkscape globe-regions-hit-areas.svg -i {} -j -e labels/{}-disabled.png
sed -i 's/fill: #DEDEDE;/fill: #000000;/gi' globe-regions-hit-areas.svg

#optipng labels/source-*.png
