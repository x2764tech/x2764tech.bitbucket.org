(function() {
  define(function(_, module) {
    var chartSegments, dataInner, dataOuter, globeWidth, height, labelPadding, width, yearLineOffset;
    width = 615;
    height = 615;
    globeWidth = 195;
    dataInner = 215;
    dataOuter = 514;
    chartSegments = 8;
    labelPadding = 6;
    yearLineOffset = 20;
    return module = {
      width: width,
      halfWidth: width / 2,
      height: height,
      halfHeight: height / 2,
      dataInner: dataInner,
      dataOuter: dataOuter,
      globeWidth: globeWidth,
      dataInnerRadius: dataInner / 2,
      dataOuterRadius: dataOuter / 2,
      chartOuterRadius: (((dataOuter - dataInner) / chartSegments) * (chartSegments - 1) + dataInner) / 2,
      labelPadding: labelPadding,
      yearLineOffset: yearLineOffset
    };
  });

}).call(this);
