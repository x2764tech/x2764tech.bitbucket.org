(function() {
  define(['sizes', 'trigonometry', 'd3'], function(sizes, trigonometry, d3) {
    var pointTypes;
    pointTypes = {
      CURVED: 0,
      HORIZONTAL: 1,
      VERTICAL: 2,
      getPointType: function(prev, current, next) {
        if (current.x === next.x && current.x === prev.x) {
          return pointTypes.HORIZONTAL;
        } else if (current.y === next.y && current.y === prev.y) {
          return pointTypes.VERTICAL;
        } else {
          return pointTypes.CURVED;
        }
      },
      getPointTypeAt: function(axis, length) {
        var current, next, prev;
        current = axis.getPointAtLength(length);
        prev = length > 0 ? axis.getPointAtLength(length - 1) : current;
        next = length < axis.getTotalLength() ? axis.getPointAtLength(length + 1) : current;
        return this.getPointType(prev, current, next);
      }
    };
    return {
      sizes: sizes,
      trigonometry: trigonometry,
      d3: d3,
      transition: function(d) {
        return d.transition().duration(500).ease('cubic-out');
      },
      isEven: function(d) {
        return d % 2 === 0;
      },
      isOdd: function(d) {
        return d % 2 === 1;
      },
      identity: function(d) {
        return d;
      },
      pointTypes: pointTypes
    };
  });

}).call(this);
