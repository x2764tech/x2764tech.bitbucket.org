
/* @license copyright 2013 x2764tech limited */

(function() {
  requirejs.config({
    paths: {
      'd3': '../lib/d3.v3',
      'topojson.min': '../lib/topojson.min'
    },
    shim: {
      'd3': {
        exports: 'd3'
      },
      'topojson.min': {
        exports: 'topojson'
      }
    }
  });

  require(['classes/globe', 'classes/datasourcemanager', 'classes/regionoverlaycontroller', 'common', 'classes/globeapp', 'classes/piebar', 'classes/yearaxis', 'classes/labels', 'ga'], function(Globe, DataSourceManager, RegionOverlayController, common, GlobeApp, PieBar, YearAxis, Labels, trackEvent) {
    var appController, changeUnitsTo, chartArea, chartAxis, chartCombo, countryScale, d3, dataOuter, dataOuterRadius, dataSourceTitle, dataSourceUnit, dataSources, globeGroup, halfHeight, halfWidth, height, hideUnits, labelArea, nonCountryScale, pieBar, ref, regionOverlay, regionOverlayController, regions, regionsClickers, setupTracking, showActiveRegion, showGlobe, showUnits, sizes, sourceLabels, svg, transition, trigonometry, updateYearLabels, wait, width, yearAxisController, yearEvents, yearLineOffset;
    ref = [common.d3, common.sizes, common.trigonometry, common.transition], d3 = ref[0], sizes = ref[1], trigonometry = ref[2], transition = ref[3];
    d3.selection.prototype["class"] = function(className) {
      return this.classed(className, function() {
        return true;
      });
    };
    d3.selection.prototype.hide = function() {
      return this.style("display", "none");
    };
    d3.selection.prototype.show = function() {
      return this.style("display", "inline");
    };
    width = sizes.width;
    halfWidth = sizes.halfWidth;
    height = sizes.height;
    halfHeight = sizes.halfHeight;
    dataOuter = sizes.dataOuter;
    dataOuterRadius = sizes.dataOuterRadius;
    changeUnitsTo = function(datasource) {
      dataSourceTitle.text(datasource.Title);
      return dataSourceUnit.text(datasource.Unit);
    };
    wait = function(pause, what) {
      return setTimeout(what, pause);
    };
    svg = d3.select("#container").select("svg").attr("width", (width + 20) + "px").attr("height", (height + 20) + "px").attr("viewBox", "0px 0px " + (width + 20) + "px " + (height + 20) + "px").attr("preserveAspectRatio", "xMinYMin slice");
    svg = svg.append("g").attr("transform", "translate(20,20)");
    regionsClickers = d3.select("#container").append("svg").attr("id", "region-clickers").attr("width", (width + 20) + "px").attr("height", (height + 20) + "px").attr({
      "style": "position:absolute;top:20px;left:20px;display:none"
    });
    regionsClickers.append("image").attr({
      id: "region-close",
      x: 564 - 139,
      y: 500,
      width: 139,
      height: 18,
      "xlink:href": "images/back-to-globe-view.png"
    });
    chartCombo = svg.append("g").attr("transform", "translate( " + halfWidth + ", " + halfHeight + " )").append("g").attr("id", "chart-group");
    chartAxis = chartCombo.append("g").attr("id", "chartAxis");
    appController = new GlobeApp(chartCombo, sizes);
    appController.drawChartAxis(chartAxis, sizes.dataInnerRadius, sizes.dataOuterRadius, sizes.chartOuterRadius);
    chartArea = chartCombo.append("g").attr("id", "chart");
    labelArea = chartCombo.append("g").attr("id", "labels");
    globeGroup = svg.append("g").attr("transform", "translate( " + halfWidth + ", " + halfHeight + " )");
    appController.dataLabelArea = labelArea;
    appController.showOptionThree();
    appController.showPercentageIndicator();
    yearLineOffset = sizes.yearLineOffset;
    yearEvents = d3.dispatch("yearchange", "sourcechange", "regionchange", "regionclose");
    nonCountryScale = d3.scale.ordinal().range(["#F7D117", "#D42E12", "#EEACA0", "#003882", "#0099BA", "#8CCCD9", "#BAD405", "#00824A", "#99CDB7", "#611759", "#BA91BA", "#EDDDC1", "#DE8703", "#70331F"]);
    countryScale = d3.scale.ordinal().range(['#e68272', '#f7d118', '#6688b5', '#66c2d7', '#bbd406', '#67b492', '#a1749b', '#d6bcd5', '#de8702', '#a88478', '#c69e7b']);
    pieBar = new PieBar(chartArea, labelArea);
    dataSources = DataSourceManager.init(countryScale, nonCountryScale, function() {
      var primaryEnergy;
      primaryEnergy = dataSources.get('primaryEnergy');
      pieBar.showData(primaryEnergy, primaryEnergy.years[primaryEnergy.years.indexOf('2010')]);
      return changeUnitsTo(primaryEnergy);
    });
    dataSourceTitle = svg.append("text").attr("id", "datasource-title").attr({
      y: yearLineOffset * 4 - 12,
      x: yearLineOffset
    });
    dataSourceUnit = svg.append("text").attr("id", "datasource-unit").attr({
      y: yearLineOffset * 4,
      x: yearLineOffset
    });
    sourceLabels = (function(svg, yearLineOffset) {
      var createSourceLinePath, defs, labelly, sourceArea, sourcePaths;
      sourceArea = svg.append("g").attr("id", "source-menu");
      createSourceLinePath = function(offset) {
        var sourceEndPoint;
        if (offset == null) {
          offset = yearLineOffset;
        }
        sourceEndPoint = trigonometry.polarToCartesian(halfWidth, halfHeight, 80, halfWidth - offset);
        return "M" + offset + "," + offset + " H" + halfWidth + " A" + (halfWidth - offset) + " " + (halfHeight - offset) + " 0 0 1 " + sourceEndPoint.x + " " + sourceEndPoint.y;
      };
      defs = svg.select("defs");
      sourcePaths = sourceArea.append("g");
      sourcePaths.append("g")["class"]("source-path").append("path").attr("id", "source-path").attr("d", createSourceLinePath());
      labelly = sourceArea.append("g")["class"]("selector-label");
      labelly.append("image").attr({
        "xlink:href": "images/option-1.png",
        "width": "165",
        "height": "45",
        "x": -10,
        "y": -14
      });
      return Labels.create("#source-hit-areas", "source", sourcePaths, yearEvents.sourcechange);
    })(svg, yearLineOffset);
    sourceLabels.show();
    sourceLabels.copyClickAreasTo(regionsClickers);
    d3.select(sourceLabels.sourceSvg).selectAll("text").remove();
    yearAxisController = new YearAxis(svg.append("g").attr("id", "yearAxisArea"), yearLineOffset, yearEvents, dataSources, regionsClickers);
    yearAxisController.showYearLabels();
    regionOverlay = d3.select('#region-overlay').style("top", ((halfHeight - dataOuterRadius) + 20) + "px").style("left", ((halfWidth - dataOuterRadius) + 20) + "px").style("width", dataOuter + "px");
    regionOverlay.select("#region-chart").append("g");
    d3.select("#region-close").on("click", function() {
      return yearEvents.regionclose();
    });
    regionOverlayController = new RegionOverlayController(regionOverlay, dataOuter);
    showActiveRegion = function(region) {
      return yearAxisController.regionLabels.makeActive(region);
    };
    regionOverlayController.visible = false;
    showGlobe = function(node, data) {
      var regionInfo;
      data = data.data;
      regionInfo = dataSources.get('regions')[data.region];
      if (!regionInfo) {
        return;
      }
      regionOverlayController.visible = true;
      globeApp.globe.explode(regionInfo.Lat, regionInfo.Long);
      transition(chartCombo).attr("transform", "scale(4.5)").style("opacity", 0);
      transition(regionOverlay.style("display", "block").style("opacity", 0)).style("opacity", 1);
      regionOverlayController.showData(regionInfo, pieBar.source);
      yearAxisController.showRegionLabels();
      showActiveRegion(data.region);
      sourceLabels.enable(function(name) {
        return dataSources.get(name).forCountries;
      });
      return hideUnits();
    };
    pieBar.events.on('yearchanged', function(newYear) {
      return yearAxisController.yearLabels.makeActive(newYear);
    });
    pieBar.events.on('sourcechanged', function(newSource) {
      return sourceLabels.makeActive(newSource.name);
    });
    pieBar.events.on('areaClick', showGlobe);
    pieBar.events.on('labelClick', showGlobe);
    yearEvents.on('regionclose', function() {
      d3.event.preventDefault();
      globeApp.globe.implode();
      regionOverlayController.visible = false;
      transition(chartCombo).attr("transform", "scale(1)").style("opacity", 1);
      transition(regionOverlay).style("opacity", 0).each("end", function() {
        return d3.select(this).style("display", "none");
      });
      yearAxisController.showYearLabels();
      sourceLabels.enable(function() {
        return true;
      });
      updateYearLabels();
      return showUnits();
    });
    yearEvents.on('yearchange', function(newYear) {
      return pieBar.showYear(newYear);
    });
    updateYearLabels = function() {
      var ref1, ref2, ref3;
      if (regionOverlayController.visible) {
        return;
      }
      if ((ref1 = pieBar.source) != null ? ref1.hasOceanData : void 0) {
        return yearAxisController.showOceans();
      } else {
        yearAxisController.hideOceans();
        if (((ref2 = pieBar.year) != null ? ref2[0] : void 0) === 'O') {
          yearAxisController.yearLabels.makeActive(pieBar.year.replace(/^O/, "M"));
        }
        return yearAxisController[((ref3 = pieBar.source) != null ? ref3.hasOceanData : void 0) ? "showOceans" : "hideOceans"]();
      }
    };
    hideUnits = function() {
      dataSourceTitle.hide();
      return dataSourceUnit.hide();
    };
    showUnits = function() {
      dataSourceTitle.show();
      return dataSourceUnit.show();
    };
    yearEvents.on('sourcechange', function(source) {
      var datasource;
      datasource = dataSources.get(source);
      if (!datasource) {
        return;
      }
      regionOverlayController.showSource(datasource);
      pieBar.showSource(datasource);
      updateYearLabels();
      if (datasource.layout.hidePercentageIndicator) {
        appController.hidePercentageIndicator();
      } else {
        appController.showPercentageIndicator();
      }
      if (datasource.forCountries) {
        appController.showOptionThree();
      } else {
        appController.hideOptionThree();
      }
      if (datasource.layout.hideGridLines) {
        appController.innerTicks.hide();
      } else {
        appController.innerTicks.show();
      }
      return changeUnitsTo(datasource);
    });
    yearEvents.on('regionchange', function(region) {
      var regionInfo;
      regionInfo = dataSources.get('regions')[region];
      globeApp.globe.rotate(regionInfo.Lat, regionInfo.Long);
      regionOverlayController.showRegion(regionInfo);
      return showActiveRegion(region);
    });
    regions = dataSources.add({}, 'regions');
    d3.csv('data/regions.csv', function(error, data) {
      return data.forEach(function(d) {
        regions[d.Year] = d;
        return d.Name = d.Year;
      });
    });
    this.globeApp = {
      pieBar: pieBar,
      globe: new Globe(globeGroup, sizes.globeWidth),
      dataSources: dataSources,
      appController: appController
    };
    this.globeApp.globe.on('loaded', function() {
      return this.continuallyRotate();
    });
    setupTracking = function() {
      var track, trackRegion;
      track = function() {
        return trackEvent.apply(null, arguments);
      };
      yearEvents.on("sourcechange._ga", function(s) {
        var ref1;
        return track("sourceChanged", (ref1 = dataSources.get(s)) != null ? ref1.name : void 0);
      });
      yearEvents.on("yearchange._ga", function(s) {
        return track("yearChanged", s);
      });
      trackRegion = function(s) {
        if (pieBar.source.forCountries) {
          return track("regionShown", d3.select(s).datum().data.region);
        }
      };
      pieBar.events.on('areaClick._ga', trackRegion);
      pieBar.events.on('labelClick._ga', trackRegion);
      yearEvents.on("regionchange._ga", function(r) {
        return track("regionChanged", r);
      });
      return yearEvents.on("regionclose._ga", function() {
        return track("overlayClosed");
      });
    };
    return setupTracking();
  });

}).call(this);
