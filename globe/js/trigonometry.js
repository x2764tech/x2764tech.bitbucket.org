(function() {
  define(function(_, module) {
    var degToRad;
    degToRad = function(deg) {
      return (deg - 90) * (Math.PI / 180);
    };
    return module = {
      degToRad: degToRad,
      radToDeg: function(rad) {
        return (rad * (180 / Math.PI)) - 90;
      },
      polarToCartesian: function(x, y, angle, distance) {
        var radians;
        radians = degToRad(angle);
        return {
          x: x + distance * Math.cos(radians),
          y: y + distance * Math.sin(radians)
        };
      },
      roundPoint: function(point, places) {
        return {
          x: +point.x.toFixed(places),
          y: +point.y.toFixed(places)
        };
      }
    };
  });

}).call(this);
