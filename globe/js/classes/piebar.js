(function() {
  define(['common'], function(common) {
    var PieBar, d3, ref, transition;
    ref = [common.d3, common.transition], d3 = ref[0], transition = ref[1];
    return PieBar = (function() {
      function PieBar(chartArea, labelArea) {
        this.chartArea = chartArea;
        this.labelArea = labelArea;
        this.events = d3.dispatch("yearchanged", "sourcechanged", "areaClick", "labelClick");
      }

      PieBar.prototype.setDataTo = function(source, year) {
        var circleScale, g, layout, quantizedTicks, self, textLabels, ticks;
        layout = source.layout.createYearLayout(year);
        g = this.chartArea.selectAll("path.segment").data(layout, function(d) {
          return d.data.key;
        });
        self = this;
        g.enter().append("path")["class"]("segment").attr("d", function(d) {
          return d.data.layout.createPath(d);
        }).style("fill", function(d) {
          return source.scale(d.data.ordinal);
        }).on("click", function(d) {
          return self.events.areaClick(this, d);
        });
        transition(g).attrTween("d", function(d) {
          return d.data.layout.tween(d);
        });
        transition(g.exit()).attrTween("d", function(d) {
          return d.data.layout.tweenOut(d);
        }).each("end", (function(_this) {
          return function(d) {
            return d3.select(_this).remove();
          };
        })(this));
        g = this.labelArea.selectAll("g.data-label").data(layout, function(d) {
          return d.data.key;
        });
        g.enter().append("g").attr("class", "data-label").style("font-size", "11px").style("opacity", "0").attr("data-label", function(d) {
          return d.data.region;
        }).on('click', function(d) {
          return self.events.labelClick(this, d);
        }).each(function(d) {
          return d.data.layout.labelLayout.split(d.data.region, this);
        }).attr("transform", function(d) {
          return d.data.layout.labelLayout.initialTransform(this, d);
        });
        transition(g).attrTween("transform", function(d) {
          return d3.interpolateString(d3.select(this).attr("transform"), d.data.layout.labelLayout.layoutTransform(this, d));
        }).style("opacity", function(d) {
          if (d.data.layout.labelLayout.shouldShow(this, d)) {
            return "1";
          } else {
            return "0";
          }
        });
        transition(g.exit()).attrTween("transform", function(d) {
          return d3.interpolateString(d3.select(this).attr("transform"), d.data.layout.labelLayout.exitTransform(this, d));
        }).style("opacity", 0).remove();
        ticks = source.layout.createTicks();
        quantizedTicks = ticks.map(source.quantize);
        circleScale = this.labelArea.selectAll("circle.scale.marker").data(quantizedTicks, function(_, i) {
          return i;
        });
        circleScale.enter().append("circle")["class"]("scale marker").attr({
          "cx": 0
        }).attr({
          "cy": function(d) {
            return 0 - d;
          }
        }).style("opactity", 0).attr("r", 2);
        transition(circleScale).attr("cy", function(d) {
          return 0 - d;
        }).style("opactity", 1);
        circleScale.exit().remove();
        textLabels = this.labelArea.selectAll("text.scale.marker").data(ticks);
        textLabels.enter().append("text")["class"]("scale marker").text(function(d) {
          return d;
        }).attr("x", 0).attr("text-anchor", "middle").attr("y", function(_, i) {
          return 6 - quantizedTicks[i];
        }).attr({
          "dy": ".71em"
        }).style("opactity", 0);
        transition(textLabels).attr("y", function(_, i) {
          return 6 - quantizedTicks[i];
        }).style("opactity", 1).tween("text", function(d) {
          var i;
          i = d3.interpolate(this.textContent, d);
          return function(t) {
            return this.textContent = Math.round(i(t));
          };
        });
        return textLabels.exit().remove();
      };

      PieBar.prototype._sortByOrdinal = function(a, b) {
        if (a.ordinal < b.ordinal) {
          return -1;
        }
        if (a.ordinal > b.ordinal) {
          return 1;
        }
        return 0;
      };

      PieBar.prototype.showData = function(source, year) {
        if (this.source === source && this.year === year) {
          return false;
        }
        this.setDataTo(source, year);
        if (this.year !== year) {
          this.events.yearchanged(year);
        }
        this.year = year;
        if (this.source !== source) {
          this.events.sourcechanged(source);
        }
        return this.source = source;
      };

      PieBar.prototype.showYear = function(year) {
        return this.showData(this.source, year);
      };

      PieBar.prototype.showSource = function(source) {
        return this.showData(source, this.year);
      };

      return PieBar;

    })();
  });

}).call(this);
