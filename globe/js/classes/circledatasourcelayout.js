(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  define(['./circlelabellayout', './datasourcelayout', 'common'], function(CircleLabelLayout, DataSourceLayout, common) {
    var CircleDataSourceLayout, ref, sizes, trigonometry;
    ref = [common.trigonometry, common.sizes], trigonometry = ref[0], sizes = ref[1];
    CircleDataSourceLayout = (function() {
      CircleDataSourceLayout.maxDataWidth = ((sizes.chartOuterRadius - sizes.dataInnerRadius) / 2) * 0.95;

      CircleDataSourceLayout.circleCenter = sizes.dataInnerRadius + (sizes.chartOuterRadius - sizes.dataInnerRadius) / 2;

      function CircleDataSourceLayout(datasource) {
        this.datasource = datasource;
        this.createYearLayout = bind(this.createYearLayout, this);
        this._tweenOut = bind(this._tweenOut, this);
        this._tween = bind(this._tween, this);
        this.createPath = bind(this.createPath, this);
        this.circleCenter = CircleDataSourceLayout.circleCenter;
        this.quantize = d3.scale.linear().domain([0, this.datasource.max]).range([0, CircleDataSourceLayout.maxDataWidth]);
        this.tweenIn = this.tween = this._tween;
        this.tweenOut = this._tweenOut;
        this.hidePercentageIndicator = true;
        this.hideGridLines = true;
      }

      CircleDataSourceLayout.prototype.createPath = function(d) {
        var center;
        center = trigonometry.polarToCartesian(0, 0, d.angle, this.circleCenter);
        return this._createCirclePath(center.x, center.y, d.quantizedValue);
      };

      CircleDataSourceLayout.prototype._clone = function(datum) {
        return {
          value: datum.value,
          quantizedValue: datum.quantizedValue,
          value: datum.value
        };
      };

      CircleDataSourceLayout.prototype._tween = function(newData) {
        var interpolator, last, region;
        region = newData.data.key;
        newData.quantizedValue = this.quantize(newData.value);
        last = DataSourceLayout.previous[region] || {
          value: 0,
          quantizedValue: this.quantize(0),
          angle: newData.angle
        };
        interpolator = d3.interpolate(last, this._clone(newData));
        return (function(_this) {
          return function(t) {
            var interpolated;
            interpolated = interpolator(t);
            interpolated.data = newData.data;
            return _this.createPath(DataSourceLayout.previous[region] = interpolated);
          };
        })(this);
      };

      CircleDataSourceLayout.prototype._tweenOut = function(newData) {
        return this._tween({
          value: 0,
          angle: newData.angle,
          data: newData.data
        });
      };

      CircleDataSourceLayout.prototype._createCirclePath = function(x, y, radius) {
        return "M " + x + "," + y + " m " + (-radius) + ",0 a " + radius + "," + radius + " 0 1 1 " + (2 * radius) + ",0 a " + radius + "," + radius + " 0 1 1 " + (-2 * radius) + ",0 ";
      };

      CircleDataSourceLayout.prototype.createYearLayout = function(year) {
        var degressPerDatum, index;
        index = 0;
        degressPerDatum = 360 / this.datasource.data.length;
        return this.datasource.data.map((function(_this) {
          return function(d) {
            var value;
            value = d.get(year);
            return {
              value: value,
              data: d,
              angle: (++index) * degressPerDatum,
              quantizedValue: _this.quantize(value)
            };
          };
        })(this));
      };

      CircleDataSourceLayout.prototype.createTicks = function() {
        return [];
      };

      CircleDataSourceLayout.prototype.remove = function(data) {
        return delete DataSourceLayout.previous[data.key];
      };

      return CircleDataSourceLayout;

    })();
    CircleDataSourceLayout.prototype.labelLayout = new CircleLabelLayout(CircleDataSourceLayout.circleCenter, CircleDataSourceLayout.maxDataWidth);
    return CircleDataSourceLayout;
  });

}).call(this);
