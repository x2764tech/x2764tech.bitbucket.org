(function() {
  define(['./textsplitter', 'common'], function(TextSplitter, common) {
    var PieLabelLayout, ref, sizes, trigonometry;
    ref = [common.trigonometry, common.sizes], trigonometry = ref[0], sizes = ref[1];
    return PieLabelLayout = (function() {
      function PieLabelLayout(radius) {
        this.radius = radius;
        this.textSplitter = new TextSplitter(sizes.dataInnerRadius + 2 * sizes.labelPadding);
      }

      PieLabelLayout.prototype.shouldFlip = function(angle) {
        return angle > 90 && angle <= 270;
      };

      PieLabelLayout.prototype.split = function(text, node) {
        var bbox, canvas, ctx, getBack, i, j, pos, pt, ref1, style, textContent, txt;
        this.textSplitter.layout(text, node);
        txt = node.querySelector("text");
        getBack = txt.getCTM().inverse();
        bbox = txt.getBBox();
        canvas = document.createElement("canvas");
        canvas.width = bbox.width;
        canvas.height = bbox.height;
        ctx = canvas.getContext("2d");
        style = window.getComputedStyle(txt);
        ctx.fillStyle = style.fill;
        ctx.strokeStyle = "rgba(255, 255, 255, 0.0)";
        ctx.font = style.fontVariant + " " + style.fontWeight + " " + style.fontSize + " " + style.fontFamily;
        textContent = txt.textContent;
        for (i = j = 0, ref1 = textContent.length - 1; j <= ref1; i = j += 1) {
          pos = txt.getStartPositionOfChar(i);
          if ((pos.y - bbox.y) > bbox.height) {
            pos = pos.matrixTransform(getBack);
          }
          ctx.fillText(textContent[i], pos.x - bbox.x, pos.y - bbox.y);
        }
        pt = d3.select(txt.parentNode);
        pt.selectAll("text").remove();
        this.trim(ctx, canvas);
        return pt.append("image")["class"]("debug-box").attr("xlink:href", canvas.toDataURL()).attr("width", canvas.width).attr("height", canvas.height).attr("y", -canvas.height).attr("x", 0);
      };

      PieLabelLayout.prototype.textTransform = function(bounds, angle) {
        var extraRotation, flip, midPoint, rotate;
        flip = this.shouldFlip(angle);
        midPoint = {
          x: bounds.width / 2,
          y: 0
        };
        extraRotation = bounds.height / (Math.PI * this.radius);
        if (flip) {
          extraRotation = 0 - extraRotation;
        }
        extraRotation *= 180 / Math.PI;
        rotate = flip ? 180 : 0;
        return "rotate(" + (angle + extraRotation) + ") translate(" + (this.radius - bounds.x) + ", 0) rotate(" + rotate + "," + midPoint.x + "," + midPoint.y + ")";
      };

      PieLabelLayout.prototype._transform = function(node, angle) {
        return this.textTransform(node.getBBox(), trigonometry.radToDeg(angle));
      };

      PieLabelLayout.prototype.initialTransform = function(node, datum) {
        return this._transform(node, datum.endAngle);
      };

      PieLabelLayout.prototype.layoutTransform = function(node, datum) {
        return this._transform(node, datum.startAngle + (datum.endAngle - datum.startAngle) / 2);
      };

      PieLabelLayout.prototype.exitTransform = function(node, datum) {
        return this._transform(node, datum.endAngle);
      };

      PieLabelLayout.prototype.shouldShow = function(node, datum) {
        var dataHeight, height;
        height = node.getBBox().height;
        dataHeight = Math.sqrt(2 * Math.pow(this.radius, 2) - (2 * Math.pow(this.radius, 2) * Math.cos(datum.endAngle - datum.startAngle)));
        return dataHeight >= height;
      };

      PieLabelLayout.prototype.trim = function(ctx, canvas) {
        var alpha, bottomRightCorner, data, height, j, k, originalHeight, originalWidth, pixelPosition, ref1, ref2, relevantData, topLeftCorner, width, x, y;
        originalWidth = canvas.width;
        originalHeight = canvas.height;
        data = ctx.getImageData(0, 0, originalWidth, originalHeight).data;
        topLeftCorner = {
          x: originalWidth + 1,
          y: originalHeight + 1
        };
        bottomRightCorner = {
          x: -1,
          y: -1
        };
        for (y = j = 0, ref1 = originalHeight - 1; j <= ref1; y = j += 1) {
          for (x = k = 0, ref2 = originalWidth - 1; k <= ref2; x = k += 1) {
            pixelPosition = x * 4 + y * originalWidth * 4;
            alpha = data[pixelPosition + 3];
            if (alpha <= 0) {
              continue;
            }
            if (x < topLeftCorner.x) {
              topLeftCorner.x = x;
            }
            if (y < topLeftCorner.y) {
              topLeftCorner.y = y;
            }
            if (x > bottomRightCorner.x) {
              bottomRightCorner.x = x;
            }
            if (y > bottomRightCorner.y) {
              bottomRightCorner.y = y;
            }
          }
          width = (bottomRightCorner.x - topLeftCorner.x) + 1;
        }
        height = (bottomRightCorner.y - topLeftCorner.y) + 1;
        relevantData = ctx.getImageData(topLeftCorner.x, topLeftCorner.y, width, height);
        canvas.width = width;
        canvas.height = height;
        ctx.clearRect(0, 0, width, height);
        ctx.putImageData(relevantData, 0, 0);
        return {
          width: width,
          height: height
        };
      };

      return PieLabelLayout;

    })();
  });

}).call(this);
