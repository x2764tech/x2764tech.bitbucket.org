(function() {
  define(['common'], function(common) {
    var bars, d3;
    d3 = common.d3;
    bars = function(data, value) {
      var align, barwidth, calcTop, count, domain, maxHeight, offset, scale, spacing, updateBarWidth, updateScale, width, x_layout;
      value = value || function(d) {
        return d;
      };
      align = "bottom";
      offset = 0;
      barwidth = 0;
      count = data.length;
      width = bars.width;
      spacing = bars.spacing;
      maxHeight = bars.height;
      updateBarWidth = function() {
        return barwidth = (width - (spacing * (count + 2))) / count;
      };
      updateBarWidth();
      calcTop = function(height) {
        if (align === "top") {
          return 0;
        } else {
          return offset + (maxHeight - height);
        }
      };
      domain = [0, d3.max(data, value)];
      scale = d3.scale.linear().domain(domain);
      updateScale = function() {
        return scale = scale.range([0, maxHeight]);
      };
      updateScale();
      x_layout = function(datum, i) {
        var bottom, height, left, right, top, v;
        v = value(datum);
        left = spacing + i * (barwidth + spacing);
        height = scale(v);
        top = calcTop(height);
        right = left + barwidth;
        bottom = top + height;
        return "M" + left + "," + top + " l0," + height + " " + barwidth + ",0 0," + (-height) + " Z";
      };
      x_layout.offset = function(o) {
        offset = o;
        return x_layout;
      };
      x_layout.width = function(w) {
        width = w;
        updateBarWidth();
        return x_layout;
      };
      x_layout.spacing = function(s) {
        if (arguments.length === 0) {
          return spacing;
        }
        spacing = s;
        updateBarWidth();
        return x_layout;
      };
      x_layout.barWidth = function() {
        return barwidth;
      };
      x_layout.height = function(h) {
        if (arguments.length === 0) {
          return maxHeight;
        }
        maxHeight = h;
        updateScale();
        return x_layout;
      };
      x_layout.align = function(a) {
        if (arguments.length === 0) {
          return align;
        }
        align = a;
        return x_layout;
      };
      x_layout.get_scale = function() {
        return scale;
      };
      x_layout.count = function() {
        return count;
      };
      x_layout.domain = function(min, max) {
        if (arguments.length === 0) {
          return domain;
        }
        domain = arguments.length === 1 ? min : [min, max];
        scale.domain(domain);
        return x_layout;
      };
      return x_layout;
    };
    bars.width = 100;
    bars.spacing = 5;
    bars.height = 80;
    return bars;
  });

}).call(this);
