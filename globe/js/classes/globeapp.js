(function() {
  define(['common'], function(common) {
    var GlobeApp, d3, pointTypes, trigonometry;
    d3 = common.d3, pointTypes = common.pointTypes, trigonometry = common.trigonometry;
    return GlobeApp = (function() {
      function GlobeApp(chartCombo, sizes) {
        this.chartCombo = chartCombo;
        this.sizes = sizes;
      }

      GlobeApp.createTextCanvas = function(textElement, color, style, weight, size, family) {
        var childNode, container, ctx, i, j, labelText, position, ref;
        if (color == null) {
          color = "black";
        }
        if (style == null) {
          style = "normal";
        }
        if (weight == null) {
          weight = "300";
        }
        if (size == null) {
          size = "15px";
        }
        if (family == null) {
          family = "Oxygen";
        }
        container = document.createElement('canvas');
        ctx = container.getContext("2d");
        container.id = "html-labels-" + id;
        container.className = 'html-labels-label';
        container.width = bounding.width + 1;
        container.height = bounding.height + 1;
        container.style.position = "absolute";
        container.style.top = bounding.y + "px";
        container.style.left = bounding.x + "px";
        ctx.fillStyle = color;
        ctx.font = style + " " + weight + " " + size + "/2 " + family;
        for (childNode in textElement.childNodes) {
          labelText = childNode.textContent;
          for (i = j = 0, ref = textElement.getNumberOfChars() - 1; j <= ref; i = j += 1) {
            ctx.save();
            position = textElement.getStartPositionOfChar(i);
            ctx.translate(position.x - bounding.x, position.y - bounding.y);
            ctx.rotate(textElement.getRotationOfChar(i) * Math.PI / 180);
            ctx.fillText(labelText[i], 0, 0);
            ctx.restore();
          }
        }
        return container;
      };

      GlobeApp.layoutLabels = function(labels, axis, offset, incrementOffset) {
        var angle, axisLength, axisParent, axisPointType, center, current, data, endPoint, halfWay, halfWayPoint, i, j, k, labelData, labelSpacing, labelsContainer, labelsLength, len, length, line, next, nextDx, pointAtI, pointType, prev, ref, results, revised, separatorHeight, spacingLabels, startPoint, svg, xLength, yLength;
        axisLength = axis.getTotalLength() - 5;
        spacingLabels = labels.filter(incrementOffset);
        labelsLength = d3.sum(spacingLabels.map(function(s) {
          return d3.sum(s.map(function(d) {
            return d.getComputedTextLength();
          }));
        }));
        labelSpacing = (axisLength - (offset + labelsLength)) / (spacingLabels.size() - 1);
        nextDx = offset;
        labelData = [];
        labels.each(function(d, i) {
          var curDx, dTextPath, dThis, myLength, results, styleKey;
          myLength = this.getComputedTextLength();
          curDx = nextDx;
          dThis = d3.select(this);
          dTextPath = dThis.select(".textpath").attr("startOffset", curDx + "px");
          if (incrementOffset(d, i)) {
            nextDx += myLength + labelSpacing;
            labelData.push({
              dx: curDx,
              length: myLength,
              element: this
            });
          }
          results = [];
          for (styleKey in this.style) {
            if (this.style.hasOwnProperty(styleKey) && 0 === styleKey.indexOf("font")) {
              results.push(dThis.style(styleKey, dThis.style(styleKey)));
            }
          }
          return results;
        });
        labelsContainer = document.querySelector('#container');
        svg = document.querySelector('#container svg');
        axisParent = d3.select(axis.parentNode);
        axisPointType = [];
        prev = next = current = 0;
        for (i = j = 0, ref = axisLength - 1; j <= ref; i = j += 1) {
          pointAtI = next || axis.getPointAtLength(i);
          prev = current || pointAtI;
          current = pointAtI;
          if (i < axisLength - 1) {
            next = axis.getPointAtLength(i + 1);
          }
          axisPointType[i] = pointTypes.getPointType(prev, next, current);
        }
        revised = labelData.slice(0);
        revised.pop();
        revised.unshift({
          dx: offset - labelSpacing,
          length: 0
        });
        revised.push({
          dx: axisLength - labelSpacing / 2,
          length: 0
        });
        results = [];
        for (k = 0, len = revised.length; k < len; k++) {
          data = revised[k];
          halfWay = Math.round(data.dx + data.length + labelSpacing / 2);
          pointType = axisPointType[halfWay >= axisLength ? axisLength - 1 : halfWay - 1];
          halfWayPoint = axis.getPointAtLength(halfWay);
          center = {
            x: common.sizes.halfWidth,
            y: common.sizes.halfHeight
          };
          separatorHeight = 20;
          line = pointType === pointTypes.VERTICAL ? {
            x1: halfWayPoint.x,
            y1: halfWayPoint.y - separatorHeight,
            x2: halfWayPoint.x,
            y2: halfWayPoint.y + separatorHeight
          } : pointType === pointTypes.HORIZONTAL ? {
            x1: halfWayPoint.x - separatorHeight,
            y1: halfWayPoint.y,
            x2: halfWayPoint.x + separatorHeight,
            y2: halfWayPoint.y
          } : (yLength = halfWayPoint.y - center.y, xLength = halfWayPoint.x - center.x, angle = common.trigonometry.radToDeg(Math.atan(yLength / xLength)), yLength < 0 || xLength > 0 ? angle += 180 : void 0, length = Math.sqrt(xLength * xLength + yLength * yLength), startPoint = common.trigonometry.polarToCartesian(center.x, center.y, angle, length + separatorHeight), endPoint = common.trigonometry.polarToCartesian(center.x, center.y, angle, length - separatorHeight), {
            x1: 0,
            y1: length + separatorHeight,
            x2: center.x,
            y2: center.y,
            transform: "rotate(" + (angle + 90) + ", " + center.x + ", " + center.y + ")",
            "data-xlength": xLength,
            "data-ylength": yLength
          });
          results.push(axisParent.append("line")["class"]("debug-box").attr(line).each(function() {
            var key, myStyle, results1;
            myStyle = window.getComputedStyle(this);
            results1 = [];
            for (key in myStyle) {
              if (this.style.hasOwnProperty(key) && key.indexOf("stroke") > -1) {
                results1.push(this.style[key] = myStyle[key]);
              }
            }
            return results1;
          }));
        }
        return results;
      };

      GlobeApp.prototype.drawChartAxis = function(chartAxis, dataInnerRadius, dataOuterRadius, chartOuterRadius) {
        var j, k, lineQuantize, ref, results, results1, tickContainer, tickDegrees, totalTicks;
        chartAxis.append("circle").attr("r", chartOuterRadius).style("fill", "#58595b");
        chartAxis.append("circle").attr("r", dataInnerRadius).style("fill", "#fff");
        lineQuantize = d3.scale.linear().domain([0, 8]).range([dataInnerRadius, dataOuterRadius]);
        chartAxis.append("g").attr("id", "chartLines").selectAll("circle").data([0, 1, 2, 3, 4, 5, 6, 7, 8]).enter().append("circle").classed("inner-tick", function(d) {
          return d < 7;
        }).attr("r", function(d) {
          return lineQuantize(d);
        }).classed("chart-line", true).classed('major', function(d) {
          return d > 7 || common.isEven(d);
        }).classed('minor', common.isOdd);
        totalTicks = 40;
        tickDegrees = 360 / totalTicks;
        tickContainer = chartAxis.append("g").attr("id", "chartTicks");
        tickContainer.selectAll("line.tick").data((function() {
          results = [];
          for (var j = 1; 1 <= totalTicks ? j <= totalTicks : j >= totalTicks; 1 <= totalTicks ? j++ : j--){ results.push(j); }
          return results;
        }).apply(this)).enter().append("line")["class"]("tick inner-tick").classed("major", function(d) {
          return d % 2 === 0;
        }).classed("minor", function(d) {
          return d % 2 === 1;
        }).attr("x1", dataInnerRadius).attr("x2", chartOuterRadius).attr("transform", function(d) {
          return "rotate( " + (d * tickDegrees - 90) + ")";
        });
        this.innerTicks = chartAxis.selectAll(".inner-tick");
        tickContainer.selectAll("line.outer-tick").data((function() {
          results1 = [];
          for (var k = 1, ref = totalTicks / 2; 1 <= ref ? k <= ref : k >= ref; 1 <= ref ? k++ : k--){ results1.push(k); }
          return results1;
        }).apply(this)).enter().append("line")["class"]("tick outer-tick major").classed("percentage", function(d) {
          return d === 10;
        }).attr("x1", dataOuterRadius).attr("x2", chartOuterRadius).attr("transform", function(d) {
          return "rotate(" + (d * 2 * tickDegrees - 90) + ")";
        });
        return this.hiddenByPercentageTick = tickContainer.selectAll(".percentage");
      };

      GlobeApp.prototype.showOptionThree = function() {
        if (this.option3 == null) {
          this.option3 = this.createOptionThreeIn();
        }
        return this.option3.show();
      };

      GlobeApp.prototype.hideOptionThree = function() {
        if (this.option3 != null) {
          return this.option3.hide();
        }
      };

      GlobeApp.prototype.createOptionThreeIn = function() {
        var position;
        position = trigonometry.polarToCartesian(0, 0, (360 / 40) * 15, this.sizes.chartOuterRadius - 35);
        return this.dataLabelArea.append("image").attr({
          "xlink:href": "images/option-3.png",
          "width": "45",
          "height": "45",
          "id": "option3",
          "x": position.x,
          "y": position.y
        });
      };

      GlobeApp.prototype.showPercentageIndicator = function() {
        this.createPercentageIndicator();
        this.percentageIndicator.show();
        return this.hiddenByPercentageTick.hide();
      };

      GlobeApp.prototype.hidePercentageIndicator = function() {
        var ref;
        if ((ref = this.percentageIndicator) != null) {
          ref.hide();
        }
        return this.hiddenByPercentageTick.show();
      };

      GlobeApp.prototype.createPercentageIndicator = function() {
        if (this.percentageIndicator != null) {
          return;
        }
        return this.percentageIndicator = this.chartCombo.append("image").attr({
          "xlink:href": "images/percentage-indicator.png",
          "width": "68",
          "height": "11",
          "id": "option3",
          "x": -34,
          "y": (this.sizes.chartOuterRadius + (this.sizes.dataOuterRadius - this.sizes.chartOuterRadius) / 2) - 6
        });
      };

      return GlobeApp;

    })();
  });

}).call(this);
