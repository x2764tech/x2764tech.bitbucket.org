(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  define(['common', './datasourcelayout'], function(common, DataSourceLayout) {
    var DataSource, d3, ref, sizes;
    ref = [common.d3, common.sizes], d3 = ref[0], sizes = ref[1];
    return DataSource = (function() {
      function DataSource(scale, name) {
        this.scale = scale;
        this.name = name;
        this.parse = bind(this.parse, this);
        this.max = 0;
        this.data = [];
        this.regions = [];
        this.years = [];
        this.layout = new DataSourceLayout(this);
        this.forCountries = true;
        this.hasOceanData = true;
      }

      DataSource.prototype.createRegionData = function(region) {
        var tgt;
        tgt = d3.map();
        tgt.layout = this.layout;
        tgt.key = tgt.region = region;
        tgt.ordinal = this.regions.push(region) - 1;
        return tgt;
      };

      DataSource.prototype.parse = function(d) {
        var region, tgt, value, year;
        region = d.Year;
        tgt = this.createRegionData(region);
        for (year in d) {
          value = d[year];
          if (!(year !== "Year")) {
            continue;
          }
          tgt.set(year, +value);
          if (indexOf.call(this.years, year) < 0) {
            this.years.push(year);
          }
          this.max = Math.max(value, this.max);
        }
        this.data.push(tgt);
        return d;
      };

      DataSource.prototype.removeEmptyData = function() {
        var d;
        this.data = (function() {
          var i, len, ref1, results;
          ref1 = this.data;
          results = [];
          for (i = 0, len = ref1.length; i < len; i++) {
            d = ref1[i];
            if (d.values().some(function(v) {
              return v > 0;
            })) {
              results.push(d);
            }
          }
          return results;
        }).call(this);
        return this.regions = (function() {
          var i, len, ref1, results;
          ref1 = this.data;
          results = [];
          for (i = 0, len = ref1.length; i < len; i++) {
            d = ref1[i];
            results.push(d.region);
          }
          return results;
        }).call(this);
      };

      DataSource.prototype._createQuantizer = function() {
        this.quantize = d3.scale.linear().domain([0, this.max]).nice(5).range([sizes.dataInnerRadius, sizes.chartOuterRadius]);
        return this.data.forEach((function(_this) {
          return function(d) {
            return d.quantize = _this.quantize;
          };
        })(this));
      };

      DataSource.prototype.load = function(url, cb) {
        d3.csv(url, this.parse, (function(_this) {
          return function() {
            _this._createQuantizer();
            _this.loaded = true;
            if (cb) {
              return cb(_this);
            }
          };
        })(this));
        return this;
      };

      return DataSource;

    })();
  });

}).call(this);
