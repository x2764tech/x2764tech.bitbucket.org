(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  define(['common', './pielabellayout'], function(common, PieLabelLayout) {
    var DataSourceLayout, d3, ref, sizes;
    ref = [common.d3, common.sizes], d3 = ref[0], sizes = ref[1];
    DataSourceLayout = (function() {
      DataSourceLayout.previous = {};

      DataSourceLayout.prototype._sortByOrdinal = function(a, b) {
        if (a.ordinal < b.ordinal) {
          return -1;
        }
        if (a.ordinal > b.ordinal) {
          return 1;
        }
        return 0;
      };

      DataSourceLayout.prototype._clonePiePoint = function(d) {
        return {
          value: d.value,
          quantizedValue: d.quantizedValue,
          startAngle: d.startAngle,
          endAngle: d.endAngle
        };
      };

      DataSourceLayout.prototype._arcTween = function(newData) {
        var interpolator, last, region;
        region = newData.data.key;
        newData.quantizedValue = newData.data.quantize(newData.value);
        last = DataSourceLayout.previous[region] || {
          startAngle: newData.endAngle - 0.001,
          endAngle: newData.endAngle,
          value: 0,
          quantizedValue: newData.data.quantize(newData.value)
        };
        interpolator = d3.interpolate(last, this._clonePiePoint(newData));
        return (function(_this) {
          return function(t) {
            var interpolated;
            interpolated = interpolator(t);
            interpolated.data = newData.data;
            return _this.createPath(DataSourceLayout.previous[region] = interpolated);
          };
        })(this);
      };

      DataSourceLayout.prototype._arcTweenOut = function(newData) {
        return this._arcTween({
          startAngle: newData.endAngle - 0.001,
          endAngle: newData.endAngle,
          value: 0,
          data: newData.data
        });
      };

      function DataSourceLayout(datasource) {
        this.datasource = datasource;
        this._arcTweenOut = bind(this._arcTweenOut, this);
        this._arcTween = bind(this._arcTween, this);
        this.tweenIn = this.tween = this._arcTween;
        this.tweenOut = this._arcTweenOut;
        this.createPath = d3.svg.arc().outerRadius((function(_this) {
          return function(d) {
            return d.quantizedValue || _this.quantize(d.value);
          };
        })(this)).innerRadius(sizes.dataInnerRadius);
      }

      DataSourceLayout.prototype.createYearLayout = function(year) {
        var layout, pie;
        pie = d3.layout.pie().sort(this._sortByOrdinal).value(function(d) {
          return d.get(year);
        });
        layout = pie(this.datasource.data);
        layout.forEach((function(_this) {
          return function(d) {
            d.quantizedValue = _this.datasource.quantize(d.value);
            return d.year = year;
          };
        })(this));
        return layout;
      };

      DataSourceLayout.prototype.remove = function(data) {
        return delete DataSourceLayout.previous[data.key];
      };

      DataSourceLayout.prototype.createTicks = function() {
        return this.datasource.quantize.ticks(5).slice(1);
      };

      return DataSourceLayout;

    })();
    DataSourceLayout.prototype.labelLayout = new PieLabelLayout(sizes.dataInnerRadius + sizes.labelPadding);
    return DataSourceLayout;
  });

}).call(this);
