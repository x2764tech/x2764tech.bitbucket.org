(function() {
  define(['common'], (function(_this) {
    return function(common) {
      var Labels, bgSelector, className, clickAreaClass, d3, escapeId, get, labelId, safeSelector;
      d3 = common.d3;
      bgSelector = function(labels) {
        return "." + labels.name + "-background";
      };
      get = function(labels, d) {
        return d[labels.name];
      };
      labelId = function(labels, d) {
        return labels.name + "-" + (get(labels, d)) + "-label";
      };
      safeSelector = function(d) {
        return d.replace(/[\s\&\.]/g, "_");
      };
      escapeId = function(d) {
        return d.replace(/([\s\&\.])/g, "\\$1");
      };
      className = function(labels, d) {
        return safeSelector(get(labels, d));
      };
      clickAreaClass = function(labels) {
        return labels.name + "-click-area";
      };
      return Labels = (function() {
        Labels.create = function(id, name, parent, clicked) {
          var labels;
          labels = new Labels(Labels.getSVG(id), name, parent);
          labels.events.on("clicked", clicked);
          return labels;
        };

        Labels.getSVG = function(id) {
          return document.querySelector(id).getSVGDocument();
        };

        function Labels(sourceSvg, name1, parent1) {
          var hitAreas, name;
          this.sourceSvg = sourceSvg;
          this.name = name1;
          this.parent = parent1;
          hitAreas = this.sourceSvg.querySelectorAll("g[data-" + this.name + "]");
          name = this.name;
          this.events = d3.dispatch("clicked");
          this.data = Array.prototype.map.call(hitAreas, function(g) {
            var result;
            result = {};
            result["d"] = g.querySelector("path").getAttribute("d");
            result[name] = g.getAttribute("data-" + name);
            return result;
          });
          this.separatorsPath = this.sourceSvg.querySelector("#separators");
          this.separatorsPath = this.separatorsPath != null ? [
            {
              "d": this.separatorsPath.getAttribute("d")
            }
          ] : Array.prototype.map.call(this.sourceSvg.querySelectorAll("[class=separators]"), (function(_this) {
            return function(p) {
              return {
                "d": p.getAttribute("d"),
                "class": p.id
              };
            };
          })(this));
          this.mangleValue = common.identity;
        }

        Labels.prototype.mangleValueBy = function(mangler) {
          this.mangleValue = mangler;
          return this;
        };

        Labels.prototype.enable = function(test) {
          return d3.selectAll(bgSelector(this)).classed("disabled", (function(_this) {
            return function(d) {
              return !test(d[_this.name]);
            };
          })(this));
        };

        Labels.prototype._handleClick = function(d) {
          if (d3.selectAll("#" + (escapeId(labelId(this, d)))).classed("disabled")) {
            return;
          }
          return this.events.clicked(get(this, d));
        };

        Labels.prototype.copyClickAreasTo = function(d3Target) {
          var labels, name, ref, self;
          ref = [this, this.name], self = ref[0], name = ref[1];
          labels = d3Target.selectAll("path." + (clickAreaClass(this))).data(this.data, function(d) {
            return get(self, d);
          });
          labels.show();
          return labels.enter().append("path").attr("d", function(d) {
            return d.d;
          }).attr("data-" + name, function(d) {
            return get(self, d);
          }).attr("class", function(d) {
            return (clickAreaClass(self)) + " " + name + "-" + (className(self, d));
          }).on("click", (function(_this) {
            return function(d) {
              return _this._handleClick(d);
            };
          })(this));
        };

        Labels.prototype.show = function() {
          var background, name, self, separators;
          self = this;
          name = this.name;
          if (this.separatorsPath != null) {
            separators = this.parent.selectAll("path." + name + "-separator").data(this.separatorsPath);
            separators.enter().append("path").attr("d", function(d) {
              return d.d;
            }).attr("class", function(d) {
              return name + "-separator label-separators " + d["class"];
            });
            separators.show();
          }
          this.copyClickAreasTo(this.parent);
          background = d3.select("#background-labels").selectAll(bgSelector(this)).data(this.data, function(d) {
            return get(self, d);
          });
          background.show();
          background.enter().append("div")["class"](name + "-background").attr("id", function(d) {
            return labelId(self, d);
          }).attr("data-" + name, function(d) {
            return get(self, d);
          }).each(function(d, i) {
            var bbox, path;
            path = document.querySelector("." + name + "-" + (className(self, d)));
            bbox = path.getBBox();
            this.style.top = bbox.y + "px";
            return this.style.left = bbox.x + "px";
          });
          return this;
        };

        Labels.prototype.hidden = function(test) {
          var display;
          if (test == null) {
            test = function(x) {
              return true;
            };
          }
          display = (function(_this) {
            return function(d) {
              if (test(get(_this, d))) {
                return "none";
              } else {
                return "inline";
              }
            };
          })(this);
          d3.selectAll("." + (clickAreaClass(this))).style("display", display);
          return d3.selectAll(bgSelector(this)).style("display", display);
        };

        Labels.prototype.makeActive = function(value) {
          value = this.mangleValue(value);
          return d3.selectAll(bgSelector(this)).classed('active', (function(_this) {
            return function(d) {
              return get(_this, d) === value;
            };
          })(this));
        };

        Labels.prototype.hide = function() {
          return d3.selectAll("." + (clickAreaClass(this)) + ", " + (bgSelector(this)) + ", ." + this.name + "-separator").hide();
        };

        return Labels;

      })();
    };
  })(this));

}).call(this);
