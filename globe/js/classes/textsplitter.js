(function() {
  define(['common'], function(common) {
    var TextSplitter, d3;
    d3 = [common.d3][0];
    return TextSplitter = (function() {
      function TextSplitter(maxWidth, extraAttributes) {
        this.maxWidth = maxWidth;
        this.extraAttributes = extraAttributes;
      }

      TextSplitter.prototype.layout = function(label, parentNode) {
        var actualSplitPoint, d, height, hypenSplitLength, length, naturalPoint, parent, split2, splitPoint, text;
        parent = d3.select(parentNode);
        text = parent.append("text").attr(this.extraAttributes || {}).text(label).node();
        length = text.getComputedTextLength();
        height = text.getBBox().height;
        if (length > this.maxWidth) {
          naturalPoint = label.indexOf('-');
          if (naturalPoint === -1) {
            naturalPoint = label.indexOf('/');
          }
          if (naturalPoint > -1) {
            hypenSplitLength = text.getSubStringLength(0, naturalPoint);
          }
          if (naturalPoint > -1 && hypenSplitLength > length / 3 && hypenSplitLength < this.maxWidth) {
            actualSplitPoint = naturalPoint + 1;
          } else {
            splitPoint = label.length / 3;
            actualSplitPoint = label.lastIndexOf(" ", splitPoint);
            if (actualSplitPoint < length / 7) {
              actualSplitPoint = label.indexOf(" ", splitPoint);
            }
          }
          split2 = label.substring(actualSplitPoint).search(/[^\s]/);
          d = d3.select(text).text('').selectAll("tspan").data([label.substring(0, actualSplitPoint), label.substring(actualSplitPoint + split2)]);
          d.enter().append("tspan").text(function(d) {
            return d;
          }).attr("dy", function(d, i) {
            return (i > 0 ? height : -height) + "px";
          });
          d.text(function(d) {
            return d;
          }).attr("x", "0px");
          return d.exit().remove();
        }
      };

      return TextSplitter;

    })();
  });

}).call(this);
