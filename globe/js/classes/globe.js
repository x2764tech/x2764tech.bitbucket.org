(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  define(['topojson.min', 'common'], function(topojson, common) {
    var Globe, d3, rotateTransition, sizes, transition;
    d3 = common.d3;
    rotateTransition = function(d3) {
      return d3.transition().duration(1000);
    };
    transition = common.transition;
    sizes = common.sizes;
    return Globe = (function() {
      function Globe(container, globeWidth) {
        this.globeWidth = globeWidth;
        this.implode = bind(this.implode, this);
        this.explode = bind(this.explode, this);
        this.continuallyRotate = bind(this.continuallyRotate, this);
        this.rotate = bind(this.rotate, this);
        this.display = bind(this.display, this);
        this.globeCombo = container.append("g").attr("id", "globe-group");
        this.events = d3.dispatch("loaded");
        this.sea = this.globeCombo.append("circle").attr("r", this.globeWidth / 2 - 1).attr("id", "sea").attr("transform", "scale(1,1)");
        this.globeArea = this.globeCombo.append("g").attr("id", "globe");
        this.overlay = this.globeCombo.append("circle").attr("r", this.globeWidth / 2 - 1).attr("id", "globe-overlay").attr("transform", "scale(1,1)");
        this.projection = d3.geo.orthographic().scale(this.globeWidth / 2).translate([0, 0]).clipAngle(90);
        this.path = d3.geo.path().projection(this.projection);
        d3.json("data/world.topo.json", this.display);
      }

      Globe.prototype.on = function(event, cb) {
        return this.events.on(event, cb);
      };

      Globe.prototype.display = function(error, world) {
        this.theWorld = topojson.feature(world, world.objects.subunits);
        this.globeArea.append("path").datum(this.theWorld).attr("class", "countries").attr("d", this.path);
        return this.events.loaded.apply(this);
      };

      Globe.prototype.rotate = function(lat, long, rotation) {
        if (rotation == null) {
          rotation = 0;
        }
        return rotateTransition(this.globeArea).selectAll("path.countries").attrTween("d", (function(_this) {
          return function(d) {
            var current, dest, i;
            current = _this.projection.rotate();
            dest = [-long, -lat, rotation];
            i = d3.interpolateArray(current, dest);
            return function(t) {
              _this.projection.rotate(i(t));
              return _this.path(_this.theWorld);
            };
          };
        })(this));
      };

      Globe.prototype.continuallyRotate = function() {
        var current;
        if (this.exploded || window.location.hostname === "localhost" || /\d{1,3}(\.\d{1,3}){3}/.test(window.location.hostname)) {
          return;
        }
        current = this.projection.rotate();
        return this.rotate(0, (-current[0]) + 90).duration(5000).ease("linear").each("end", this.continuallyRotate);
      };

      Globe.prototype.explode = function(lat, long) {
        var max;
        max = sizes.dataOuter / this.globeWidth;
        transition(this.overlay).style("opacity", 0).attrTween("transform", (function(_this) {
          return function() {
            return d3.interpolateString(_this.overlay.attr("transform"), "scale(" + max + ", " + max + ")");
          };
        })(this));
        transition(this.sea).style("opacity", 0).attrTween("transform", (function(_this) {
          return function() {
            return d3.interpolateString(_this.sea.attr("transform"), "scale(" + max + ", " + max + ")");
          };
        })(this));
        this.exploded = true;
        return transition(this.globeArea).style("fill", "#dddee0").selectAll("path.countries").attrTween("d", (function(_this) {
          return function(d) {
            var i, scale;
            scale = d3.interpolate(_this.globeWidth / 2, sizes.dataOuterRadius);
            i = d3.interpolate(_this.projection.rotate(), [-long, -lat]);
            return function(t) {
              _this.projection.rotate(i(t));
              _this.projection.scale(scale(t));
              return _this.path(_this.theWorld);
            };
          };
        })(this));
      };

      Globe.prototype.implode = function() {
        transition(this.overlay).style("opacity", 1).attr("transform", "scale(1,1)");
        transition(this.sea).style("opacity", 1).attr("transform", "scale(1,1)");
        return transition(this.globeArea).style("fill", this.globeFill).selectAll("path.countries").attrTween("d", (function(_this) {
          return function(d) {
            var scale;
            scale = d3.interpolate(sizes.dataOuterRadius, _this.globeWidth / 2);
            return function(t) {
              _this.projection.scale(scale(t));
              return _this.path(_this.theWorld);
            };
          };
        })(this)).each("end", (function(_this) {
          return function() {
            _this.exploded = false;
            return _this.continuallyRotate();
          };
        })(this));
      };

      return Globe;

    })();
  });

}).call(this);
