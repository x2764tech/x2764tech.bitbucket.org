(function() {
  define(['./textsplitter', '../trigonometry'], function(TextSplitter, trigonometry) {
    var CircleLabelLayout;
    return CircleLabelLayout = (function() {
      function CircleLabelLayout(circleCenter, maxLabelWidth) {
        this.circleCenter = circleCenter;
        this.textSplitter = new TextSplitter(maxLabelWidth, {
          'text-anchor': 'middle'
        });
      }

      CircleLabelLayout.prototype.split = function(text, node) {
        return this.textSplitter.layout(text, node);
      };

      CircleLabelLayout.prototype._transform = function(node, datum) {
        var bBox, center, yPosition;
        bBox = node.getBBox();
        center = trigonometry.polarToCartesian(0, 0, datum.angle, this.circleCenter);
        yPosition = bBox.height > datum.quantizedValue * 2 ? datum.quantizedValue - bBox.y : -(bBox.height / 2 + bBox.y);
        return "translate(" + center.x + "," + center.y + ") translate(" + (-(bBox.width / 2 + bBox.x)) + "," + yPosition + ")";
      };

      CircleLabelLayout.prototype.initialTransform = function(node, datum) {
        return this._transform(node, datum);
      };

      CircleLabelLayout.prototype.layoutTransform = function(node, datum) {
        return this._transform(node, datum);
      };

      CircleLabelLayout.prototype.exitTransform = function(node, datum) {
        return this._transform(node, datum);
      };

      CircleLabelLayout.prototype.shouldShow = function(node, datum) {
        return true;
      };

      return CircleLabelLayout;

    })();
  });

}).call(this);
