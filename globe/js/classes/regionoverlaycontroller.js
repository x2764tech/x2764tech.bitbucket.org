(function() {
  define(['common', 'classes/bars'], function(common, bars) {
    var RegionOverlayController, d3, identity, transition;
    transition = common.transition, d3 = common.d3, identity = common.identity;
    return RegionOverlayController = (function() {
      function RegionOverlayController(regionOverlay, containerWidth) {
        this.regionOverlay = regionOverlay;
        this.containerWidth = containerWidth;
        this.source = null;
        this.regionInfo = null;
      }

      RegionOverlayController.prototype._getData = function(data, test) {
        var j, key, len, ref, results;
        ref = data.keys();
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
          key = ref[j];
          if (test(key)) {
            results.push({
              key: key,
              value: data.get(key)
            });
          }
        }
        return results;
      };

      RegionOverlayController.prototype._drawBars = function(data, key, align) {
        var bar, barArea, theChart;
        if (align == null) {
          align = "bottom";
        }
        theChart = this.regionsChart.select("#" + key);
        bar = bars(data, function(d) {
          return d.value;
        }).width(this.chartWidth).align(align).spacing(7).domain(this.domain);
        barArea = theChart.selectAll(".bar").data(data, function(d) {
          return d.key;
        });
        barArea.enter().append("path").attr("class", "bar").attr("d", bar);
        transition(barArea).attr("d", bar);
        barArea.exit().remove();
        return bar;
      };

      RegionOverlayController.prototype._setUpChartArea = function() {
        var labels, maxLabelLength;
        this.regionsChart = this.regionOverlay.select("#region-chart");
        this.chartWidth = this.containerWidth;
        labels = this.regionsChart.selectAll(".label").data(['Mountains', 'Oceans']);
        labels.enter().append("text").attr("class", "label").text(function(d) {
          return d.toUpperCase();
        }).attr("dy", "0.35em").attr("y", function(d, i) {
          return bars.height + (i * 40);
        });
        maxLabelLength = d3.max(labels, function(labelList) {
          return d3.max(labelList, function(label) {
            return label.getComputedTextLength();
          });
        });
        this.chartWidth = this.containerWidth - maxLabelLength;
        labels.attr("x", this.chartWidth);
        return this.charAreas = this.regionsChart.selectAll(".chart").data(['mountains', 'oceans']).enter().append("g")["class"]("chart").attr("id", identity).attr("width", this.chartWidth).attr("height", bars.height).attr("transform", function(d, i) {
          return "translate(0," + (i * 120) + ")";
        });
      };

      RegionOverlayController.prototype._drawAxis = function(bar, mountains, oceans) {
        var axis, barWidth, count, entry, j, ref, results, spacing, tickLocations;
        spacing = bar.spacing();
        barWidth = bar.barWidth();
        count = Math.max(mountains.length, oceans.length);
        axis = this.regionsChart.selectAll(".axis").data([0]);
        entry = axis.enter().append("g").attr("class", "axis");
        tickLocations = (function() {
          results = [];
          for (var j = 0, ref = mountains.length; 0 <= ref ? j <= ref : j >= ref; 0 <= ref ? j++ : j--){ results.push(j); }
          return results;
        }).apply(this).map(function(d, i) {
          return spacing * 0.5 + (barWidth + spacing) * i;
        });
        axis.selectAll(".domain").data([118, 82]).enter().append("line")["class"]("domain").attr("x1", spacing * 0.5).attr("x2", this.chartWidth - (spacing * 1.5)).attr("y1", identity).attr("y2", identity).each(function(d, i) {
          return axis.selectAll(".tick.ticks" + i).data(tickLocations).enter().append("line")["class"]("tick ticks" + i).attr("y1", d - 4).attr("y2", d + 4).attr("x1", identity).attr("x2", identity);
        });
        return axis.selectAll(".tick.label").data(mountains).enter().append("text")["class"]("tick label").text(function(d) {
          return d.key.replace(/^[MO]/, '');
        }).attr("y", 100).attr("dy", "0.35em").attr("x", function(d, i) {
          return spacing * 0.5 + (barWidth + spacing) * (i + 0.5);
        }).attr("text-anchor", "middle");
      };

      RegionOverlayController.prototype.showData = function(regionInfo, source) {
        var bar, data, mountains, mountainsMax, oceans, oceansMax;
        if (regionInfo === null || source === null) {
          return false;
        }
        this._setUpChartArea();
        transition(this.regionOverlay.selectAll("#region-title")).text(regionInfo.Name);
        this.regionOverlay.selectAll("#region-info").html(regionInfo.Description);
        data = source.data.filter(function(d) {
          return d.region === regionInfo.Year;
        })[0];
        if (data == null) {
          return false;
        }
        mountains = this._getData(data, function(key) {
          return key[0] !== 'O';
        });
        oceans = this._getData(data, function(key) {
          return key[0] !== 'M';
        });
        mountainsMax = d3.max(mountains.map(function(d) {
          return d.value;
        }));
        oceansMax = d3.max(oceans.map(function(d) {
          return d.value;
        }));
        this.domain = [0, d3.max([mountainsMax, oceansMax])];
        this._drawBars(mountains, 'mountains');
        bar = this._drawBars(oceans, 'oceans', "top");
        this._drawAxis(bar, mountains, oceans);
        this.regionInfo = regionInfo;
        this.source = source;
        return true;
      };

      RegionOverlayController.prototype.showRegion = function(regionInfo) {
        if (regionInfo.Name === this.regionInfo.Name) {
          return;
        }
        return this.showData(regionInfo, this.source);
      };

      RegionOverlayController.prototype.showSource = function(source) {
        if (source === this.source) {
          return;
        }
        return this.showData(this.regionInfo, source);
      };

      return RegionOverlayController;

    })();
  });

}).call(this);
