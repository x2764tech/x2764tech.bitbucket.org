(function() {
  var indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  define(['./datasource', './circledatasourcelayout'], function(DataSource, CircleDataSourceLayout) {
    var CountDown, DataSourceManager, createEnergyPerCapita, notForCountries;
    notForCountries = false;
    createEnergyPerCapita = function(dataSources, countryScale) {
      var d, e, eY, i, j, k, l, len, len1, len2, len3, p, pY, population, primaryEnergy, ref, ref1, ref2, ref3, s, tgt;
      primaryEnergy = dataSources.get('primaryEnergy');
      population = dataSources.get('population');
      if (!(primaryEnergy.loaded && population.loaded)) {
        return;
      }
      s = dataSources.add(new DataSource(countryScale, 'perCapita'));
      ref = primaryEnergy.data;
      for (i = 0, len = ref.length; i < len; i++) {
        e = ref[i];
        ref1 = population.data;
        for (j = 0, len1 = ref1.length; j < len1; j++) {
          p = ref1[j];
          if (p.region !== e.region) {
            continue;
          }
          tgt = s.createRegionData(p.region);
          tgt.key = "perCapita - " + p.region;
          ref2 = e.keys();
          for (k = 0, len2 = ref2.length; k < len2; k++) {
            eY = ref2[k];
            ref3 = p.keys();
            for (l = 0, len3 = ref3.length; l < len3; l++) {
              pY = ref3[l];
              if (pY === eY) {
                tgt.set(pY, e.get(eY) / p.get(pY));
                if (indexOf.call(s.years, pY) < 0) {
                  s.years.push(pY);
                }
                break;
              }
            }
          }
          s.max = Math.max(s.max, d3.max(tgt.values()));
          s.data.push(tgt);
          break;
        }
      }
      s.regions = (function() {
        var len4, m, ref4, results;
        ref4 = s.data;
        results = [];
        for (m = 0, len4 = ref4.length; m < len4; m++) {
          d = ref4[m];
          results.push(d.region);
        }
        return results;
      })();
      s.layout = new CircleDataSourceLayout(s);
      s.data.forEach(function(d) {
        return d.layout = s.layout;
      });
      return s._createQuantizer();
    };
    CountDown = (function() {
      function CountDown(count, cb1) {
        this.count = count;
        this.cb = cb1;
        this.done = bind(this.done, this);
      }

      CountDown.prototype.done = function() {
        if (this.count <= 0) {
          return false;
        }
        this.count -= 1;
        if (this.count === 0) {
          return this.cb.apply(null, arguments);
        }
      };

      return CountDown;

    })();
    return DataSourceManager = (function() {
      DataSourceManager.init = function(countryScale, nonCountryScale, cb) {
        var countDown, dataSources, whenDone;
        dataSources = this.dataSources = new DataSourceManager();
        whenDone = function() {
          var population, primaryEnergy;
          primaryEnergy = dataSources.get('primaryEnergy');
          primaryEnergy.Title = "Primary Energy Demand";
          primaryEnergy.Unit = "EJ/Year";
          population = dataSources.get('population');
          population.Title = "Population";
          population.Unit = "in millions";
          return cb.apply(null, arguments);
        };
        countDown = new CountDown(4, whenDone);
        dataSources.create(countryScale, 'primaryEnergy', 'data/primary-energy-per-region.csv', function(d) {
          createEnergyPerCapita(dataSources, countryScale);
          return countDown.done(d);
        });
        dataSources.create(nonCountryScale, 'perSource', 'data/primary-energy-per-source.csv', countDown.done, notForCountries);
        dataSources.create(nonCountryScale, 'perSector', 'data/total-per-sector.csv', countDown.done, notForCountries);
        dataSources.create(countryScale, 'population', 'data/population-per-region.csv', function(p) {
          p.removeEmptyData();
          p.hasOceanData = false;
          createEnergyPerCapita(dataSources, countryScale);
          return countDown.done(p);
        });
        return dataSources;
      };

      function DataSourceManager() {
        this.dataSources = {};
      }

      DataSourceManager.prototype.add = function(datasource, name) {
        return this.dataSources[name || datasource.name] = datasource;
      };

      DataSourceManager.prototype.get = function(name) {
        return this.dataSources[name];
      };

      DataSourceManager.prototype.create = function(scale, name, url, cb, forCountries) {
        var ds;
        if (forCountries == null) {
          forCountries = true;
        }
        ds = new DataSource(scale, name);
        ds.forCountries = forCountries;
        this.add(ds);
        return ds.load(url, cb);
      };

      return DataSourceManager;

    })();
  });

}).call(this);
