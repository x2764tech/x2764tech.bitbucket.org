(function() {
  define(['common', 'classes/globeapp', 'classes/labels'], function(common, GlobeApp, Labels) {
    var YearAxis, chartOuterRadius, halfHeight, halfWidth, height, ref;
    ref = common.sizes, halfHeight = ref.halfHeight, halfWidth = ref.halfWidth, height = ref.height, chartOuterRadius = ref.chartOuterRadius;
    return YearAxis = (function() {
      YearAxis.prototype.abovePosition = -4;

      YearAxis.prototype.belowPosition = 16;

      YearAxis.prototype.regionPadding = 15;

      function YearAxis(yearArea, yearLineOffset1, yearEvents, dataSources, regionClickArea) {
        var axis, lastPoint, yearAxisArea;
        this.yearArea = yearArea;
        this.yearLineOffset = yearLineOffset1;
        this.yearEvents = yearEvents;
        this.dataSources = dataSources;
        this.regionClickArea = regionClickArea;
        this.labelsContainer = this.yearArea.append("g");
        this.yearLabels = Labels.create("#year-hit-areas", "year", this.labelsContainer, (function(_this) {
          return function(year) {
            return _this.yearEvents.yearchange(year);
          };
        })(this));
        yearAxisArea = this.yearArea.append("g");
        this.yearAxis = yearAxisArea.append("path").attr("d", this.createPathData(this.yearLineOffset)).attr("id", "year-path").attr("class", "year-path");
        this.labels = this.yearArea.append("g")["class"]("selector-label");
        axis = this.yearAxis.node();
        lastPoint = axis.getPointAtLength(axis.getTotalLength());
        this.labels.append("text").text("MOUNTAINS")["class"]("year-type-label").attr("x", lastPoint.x + 7).attr("y", lastPoint.y - 5);
        this.labels.append("text").text("OCEANS")["class"]("year-type-label").attr("x", lastPoint.x + 7).each(function() {
          var size;
          size = this.getBBox();
          return d3.select(this).attr("y", lastPoint.y + size.height);
        });
        this.optionLabel = this.labels.append("image").attr({
          "xlink:href": "images/option-2.png",
          "width": "45",
          "height": "165",
          "x": 9,
          "y": halfHeight - 170
        });
      }

      YearAxis.prototype.createPathData = function(yearLineOffset) {
        return "M" + yearLineOffset + "," + (halfHeight - 150) + " V" + halfHeight + " A" + (halfWidth - yearLineOffset) + " " + (halfHeight - yearLineOffset) + " 0 0 0 " + halfWidth + " " + (height - yearLineOffset) + " H" + (halfWidth + chartOuterRadius);
      };

      YearAxis.prototype.isMountainsOrOceansYear = function(d) {
        return d[0] === 'M' || d[0] === 'O';
      };

      YearAxis.prototype._setAxisLabel = function(text) {
        return this.labels.selectAll(".text-path").text(("Select " + text).toUpperCase());
      };

      YearAxis.prototype._showYearTypeLabels = function(display) {
        if (display == null) {
          display = "inline";
        }
        return this.labels.selectAll(".year-type-label").style("display", display);
      };

      YearAxis.prototype.showYearLabels = function() {
        this.hideRegionLabels();
        this.yearLabels.show();
        this._setAxisLabel("Year");
        return this._showYearTypeLabels();
      };

      YearAxis.prototype.hideYearLabels = function() {
        return this.yearLabels.hide();
      };

      YearAxis.prototype.hideRegionLabels = function() {
        var ref1;
        if ((ref1 = this.regionLabels) != null) {
          ref1.hide();
        }
        this.regionClickArea.hide();
        return this.optionLabel.attr("xlink:href", "images/option-2.png").attr("height", 165);
      };

      YearAxis.prototype.hideOceans = function() {
        return d3.selectAll("*[data-year^=O], .year-separator.oceans, .year-type-label").style("display", "none");
      };

      YearAxis.prototype.showOceans = function() {
        return d3.selectAll("*[data-year^=O], .year-separator.oceans, .year-type-label").style("display", "inline");
      };

      YearAxis.prototype.createRegionLabels = function() {
        var _, data, regionNames, regionNamesByLowerCase;
        if (this.regionLabels) {
          return;
        }
        regionNames = ((function() {
          var ref1, results;
          ref1 = this.dataSources.get('regions');
          results = [];
          for (_ in ref1) {
            data = ref1[_];
            results.push(data);
          }
          return results;
        }).call(this)).filter(function(data) {
          return data.Order > -1;
        }).sort(function(a, b) {
          return a.Order - b.Order;
        }).map(function(a) {
          return a.Name;
        });
        regionNamesByLowerCase = {};
        regionNames.forEach(function(n) {
          return regionNamesByLowerCase[n.toLowerCase()] = n;
        });
        this.regionLabels = Labels.create("#regions-hit-areas", "region", this.regionClickArea, (function(_this) {
          return function(region) {
            return _this.yearEvents.regionchange(regionNamesByLowerCase[region]);
          };
        })(this));
        return this.regionLabels.mangleValueBy(function(v) {
          return v.toLowerCase();
        });
      };

      YearAxis.prototype.showRegionLabels = function() {
        this.hideYearLabels();
        this.regionClickArea.show();
        this.createRegionLabels();
        this.regionLabels.show();
        this._setAxisLabel("Country");
        this._showYearTypeLabels("none");
        return this.optionLabel.attr("xlink:href", "images/option-3-regions.png").attr("height", 170);
      };

      return YearAxis;

    })();
  });

}).call(this);
