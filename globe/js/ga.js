(function() {
  define(function() {
    var insertGAScript;
    window._gaq = [['_setAccount', 'UA-43335500-1'], ['_trackPageview']];
    insertGAScript = function() {
      var ga, proto;
      ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      proto = document.location.protocol;
      proto = proto === 'https:' ? 'https://ssl' : 'http://www';
      ga.src = proto + ".google-analytics.com/ga.js";
      return document.body.appendChild(ga);
    };
    insertGAScript();
    return function(action, label) {
      return window._gaq.push(['_trackEvent', 'globeApp', action, label]);
    };
  });

}).call(this);
